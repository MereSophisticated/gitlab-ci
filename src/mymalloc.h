#ifndef MYMALLOC_H
#define MYMALLOC_H
#include <stddef.h>

struct alloc_info{
    size_t alloc_size;
    int st_odsekov;
    void *prvi_prost; //potencialen odsek
};


struct odsek_info{
    size_t odsek_size;
    void *stran;
};

/* deklaracija malloc */
void* mymalloc(size_t size);

/* deklaracija myfree */
void myfree(void* ptr);

#endif //MYMALLOC_H
