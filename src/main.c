#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mymalloc.h"

int main(void){
    char *str;
    char *str2;
    char *str3;

    /* alokacija spomina */
    str = (char*) mymalloc(11);
    strcpy(str, "0123456789");
    printf("%s\n", str);

    str2 = (char*) mymalloc(11);
    strcpy(str2, "0123456789");
    printf("%s\n", str2);

    str3 = (char*) mymalloc(5000);
    strcpy(str3, "0123456789");
    printf("%s\n", str3);

    myfree(str2);
    myfree(str3);
    myfree(str);
}
