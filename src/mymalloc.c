#include "mymalloc.h"
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stddef.h>

struct alloc_info *zadnja_dodeljena_stran;
size_t preostal_prostor;

void *mymalloc(size_t size)
{

    if (size <= 0)
    {
        return NULL;
    }
    void *return_addr;
    struct alloc_info *kaz_alloc_info;
    struct odsek_info *kaz_odsek_info;

    // Potrebujemo vsaj size + meta prostor
    size_t required_size = size + sizeof(struct odsek_info);

    /* Če na strani ni dovolj  prostega prostora pokliči mmap
     in  rezerviraj novo stran */
    if (zadnja_dodeljena_stran == NULL || preostal_prostor < required_size)
    {
        // V primeru alokacije novega bloka, potrebujemo še alokacijski meta prostor
        size_t alloc_size = required_size + sizeof(struct alloc_info);
        int page_size = getpagesize();

        /* Zaokrožimo velikost na večkratnik velikosti strani
         (ker vedno dobimo dodeljene cele strani) */
        size_t alloc_over = alloc_size % page_size;

        if (alloc_over > 0)
        {
            alloc_size += page_size - alloc_over;
        }

        preostal_prostor = alloc_size - size - sizeof(struct alloc_info) - sizeof(struct odsek_info);

        void *addr = mmap(0, alloc_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

        /* Preveri ali je mmap bil uspešen */
        if (addr == MAP_FAILED)
        {
            perror("Težava pri preslikavi spomina!");
        }

        /* Napolni strukturi alloc_info in odsek info*/

        kaz_alloc_info = zadnja_dodeljena_stran = (struct alloc_info *)addr;
        kaz_odsek_info = (struct odsek_info *)(((void *)kaz_alloc_info) + sizeof(struct alloc_info));

        (*kaz_alloc_info).alloc_size = alloc_size;
        (*kaz_alloc_info).st_odsekov = 1;

        printf("\nIn if in mymalloc\n");
    }
    else
    {
        kaz_alloc_info = zadnja_dodeljena_stran;
        kaz_odsek_info = (*zadnja_dodeljena_stran).prvi_prost;

        (*kaz_alloc_info).st_odsekov++;

        printf("\nIn else in mymalloc\n");
    }

    (*kaz_alloc_info).prvi_prost = (struct odsek_info *)(((void *)kaz_odsek_info) + sizeof(struct odsek_info) + size);

    (*kaz_odsek_info).odsek_size = size;
    (*kaz_odsek_info).stran = kaz_alloc_info;

    //kazalec na alociran pomnilnik
    return_addr = ((void *)kaz_odsek_info) + sizeof(struct odsek_info);

    return return_addr;
}

void myfree(void *addr)
{
    printf("\nIn myfree\n");

    struct odsek_info *kaz_odsek_info = (struct odsek_info *)(addr - sizeof(struct odsek_info));
    struct alloc_info *kaz_alloc_info = (*kaz_odsek_info).stran;

    printf("Pred dekrementacijo: %d\n", (*kaz_alloc_info).st_odsekov);
    (*kaz_alloc_info).st_odsekov--;
    printf("Po dekrementaciji: %d\n", (*kaz_alloc_info).st_odsekov);

    if ((*kaz_alloc_info).st_odsekov == 0)
    {
        printf("Odstranjujem stran\n");
        if (munmap(kaz_alloc_info, (*kaz_alloc_info).alloc_size) == -1)
        {
            perror("Težava pri odstranitvi preslikave");
        }
    }
}
