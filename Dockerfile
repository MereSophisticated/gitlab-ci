FROM ubuntu:20.04
LABEL Name=mymalloc Version=0.0.1

RUN apt-get update && \
    apt-get -y install gcc && \
    apt-get -y install make && \
    apt-get -y install valgrind && \
    apt-get -y install cppcheck

CMD dpkg -s gcc && \
    dpkg -s make && \
    dpkg -s valgrind && \
    dpkg -s cppcheck && \
    cd /mnt/share && \
    make && \
    cppcheck main.c && \
    valgrind --leak-check=yes ./main


    
